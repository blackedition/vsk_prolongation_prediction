import click
import pandas as pd
from sklearn.model_selection import train_test_split


@click.command()
@click.argument("input_file", type=click.Path(exists=True))
@click.argument("output_filepath", type=click.Path())
def build_features(input_file: str, output_filepath: str):
    RANDOM_STATE = 42
    df = pd.read_csv(input_file)

    train = df[df['target'] != -1]
    data_for_prediction = df[df['target'] == -1]

    # Мультиколлинеарные признаки
    multicollinear_features_to_drop = ['ID', 'passport_region', 'Type_ls',
                                       'Brand', 'KBM_prol_est', 'delta_kbm',
                                       'deduct_amount', 'Exp', 'Amount - gr',
                                       'premium_car']
    # Признаки у которых нет корреляции с таргетом
    not_correlation_features = ['GAP', 'married', 'product',
                                'channel - map', 'kvs_type',
                                'Age', 'discount', 'sex',
                                'price_gr', 'OSAGO_clnt']
    # Тергет
    target = ['target']
    # Список численных признаков
    num_features = ['KBM', 'price_prc']

    # Список признаков для обучения моделей
    filtered_features = [i for i in df.columns
                         if (i not in target and
                             i not in multicollinear_features_to_drop and
                             i not in not_correlation_features)]
    # Категориальные признаки
    cat_features = [i for i in filtered_features if i not in num_features]

    # Данные для обучения
    X = train[filtered_features]
    # Таргет
    y = train[target]

    # Разделение на обучающую и тестовую выборки
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=RANDOM_STATE)

    X_train.to_csv(output_filepath + "x_train.csv", index=False)
    X_test.to_csv(output_filepath + "x_test.csv", index=False)
    y_train.to_csv(output_filepath + "y_train.csv", index=False)
    y_test.to_csv(output_filepath + "y_test.csv", index=False)


if __name__ == "__main__":
    build_features()