import click
import joblib
import pandas as pd
import xgboost as xgb
from sklearn.pipeline import Pipeline
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV
from catboost import CatBoostClassifier
from category_encoders.binary import BinaryEncoder


@click.command()
@click.argument("input_filepath", type=click.Path(exists=True))
@click.argument("modelpath", type=click.Path())
def train_model(input_filepath: str, modelpath: str):
    RANDOM_STATE = 42
    X_train = pd.read_csv(input_filepath + "x_train.csv")
    y_train = pd.read_csv(input_filepath + "y_train.csv")

    # Мультиколлинеарные признаки
    multicollinear_features_to_drop = ['ID', 'passport_region', 'Type_ls',
                                       'Brand', 'KBM_prol_est', 'delta_kbm',
                                       'deduct_amount', 'Exp', 'Amount - gr',
                                       'premium_car']
    # Признаки у которых нет корреляции с таргетом
    not_correlation_features = ['GAP', 'married', 'product',
                                'channel - map', 'kvs_type',
                                'Age', 'discount', 'sex',
                                'price_gr', 'OSAGO_clnt']
    # Тергет
    target = ['target']
    # Список численных признаков
    num_features = ['KBM', 'price_prc']

    # Список признаков для обучения моделей
    filtered_features = [i for i in df.columns
                         if (i not in target and
                             i not in multicollinear_features_to_drop and
                             i not in not_correlation_features)]
    # Категориальные признаки
    cat_features = [i for i in filtered_features if i not in num_features]

    # Препроцессор для Логистической регрессии
    numeric_preprocessor = Pipeline([
        ('standardscaler', StandardScaler())
    ])
    categorical_preprocessor = Pipeline([
        ('binaryencoder', BinaryEncoder())
    ])
    preprocessor_lr = ColumnTransformer([
        ('numeric', numeric_preprocessor, num_features),
        ('categorical', categorical_preprocessor, cat_features)
    ])

    pipe_bl = Pipeline([
        ('preprocessor', preprocessor_lr),
        ('classifier', LogisticRegression(random_state=RANDOM_STATE))
    ])

    param_grid = {}

    base_line = GridSearchCV(pipe_bl,
                             param_grid=param_grid,
                             n_jobs=-1,
                             scoring='accuracy',
                             cv=5)

    base_line.fit(X_train, y_train)

    pipe_lr = Pipeline([
        ('preprocessor', preprocessor_lr),
        ('classifier', LogisticRegression(random_state=RANDOM_STATE))
    ])

    param_grid = {
        'classifier__solver': ['lbfgs', 'liblinear']
    }

    model_lr = GridSearchCV(pipe_lr,
                            param_grid=param_grid,
                            n_jobs=-1,
                            scoring='accuracy',
                            cv=5)

    model_lr.fit(X_train, y_train)

    # Препроцессор для деревьев
    categorical_preprocessor = Pipeline([
        ('binaryencoder', BinaryEncoder())
    ])
    preprocessor_tree = ColumnTransformer([
                                           ('categorical', categorical_preprocessor, cat_features)
                                           ])

    pipe_dt = Pipeline([
        ('preprocessor', preprocessor_tree),
        ('classifier', DecisionTreeClassifier(random_state=RANDOM_STATE))
    ])

    param_grid = {
        'classifier__max_depth': [5, 10, 50, 100, 1000],
    }

    model_dt = GridSearchCV(pipe_dt,
                            param_grid=param_grid,
                            n_jobs=-1,
                            scoring='accuracy',
                            cv=5)

    model_dt.fit(X_train, y_train)

    pipe_rf = Pipeline([
        ('preprocessor', preprocessor_tree),
        ('classifier', RandomForestClassifier(random_state=RANDOM_STATE))
    ])

    param_grid = {
        'classifier__n_estimators': [5, 10, 50, 100],
        'classifier__max_depth': [5, 10, 50, 100]
    }

    model_rf = GridSearchCV(pipe_rf,
                            param_grid=param_grid,
                            n_jobs=-1,
                            scoring='accuracy',
                            cv=5)

    model_rf.fit(X_train, y_train)

    model_cbc = CatBoostClassifier(cat_features=cat_features,
                                   n_estimators=200,
                                   random_state=RANDOM_STATE,
                                   eval_metric='Accuracy',
                                   verbose=500)

    param_grid = {
        'learning_rate': [0.01, 0.05, 0.1, 1],
        'depth': [5, 10, 50, 100]
    }

    grid_search_result = model_cbc.grid_search(param_grid,
                                               X_train,
                                               y_train,
                                               cv=5)

    pipe_xgb = Pipeline([
        ('preprocessor', preprocessor_lr),
        ('classifier', xgb.XGBClassifier(n_estimators=200))
    ])

    param_grid = {
        'classifier__learning_rate': [0.01, 0.05, 0.1, 1],
        'classifier__max_depth': [5, 10, 50, 100]
    }

    model_xgb = GridSearchCV(pipe_xgb,
                             param_grid=param_grid,
                             n_jobs=-1,
                             scoring='accuracy',
                             cv=5)

    model_xgb.fit(X_train, y_train)


    joblib.dump(model_lr, modelpath + "model_linear_regression.pkl")
    joblib.dump(model_dt, modelpath + "model_decision_tree.pkl")
    joblib.dump(model_rf, modelpath + "model_random_forest.pkl")
    joblib.dump(model_cbc, modelpath + "model_catboost.pkl")


if __name__ == "__main__":
    train_model()
