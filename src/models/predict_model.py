import click
import pandas as pd
import joblib
from sklearn.metrics import accuracy_score


@click.command()
@click.argument("modelpath", type=click.Path(exists=True))
@click.argument("input_filepath", type=click.Path(exists=True))
@click.argument("output_filepath", type=click.Path())
def predict(modelpath: str, input_filepath: str):
    X_test = pd.read_csv(input_filepath + "X_test.csv")
    y_test = pd.read_csv(input_filepath + "y_test.csv")

    # Мультиколлинеарные признаки
    multicollinear_features_to_drop = ['ID', 'passport_region', 'Type_ls',
                                       'Brand', 'KBM_prol_est', 'delta_kbm',
                                       'deduct_amount', 'Exp', 'Amount - gr',
                                       'premium_car']
    # Признаки у которых нет корреляции с таргетом
    not_correlation_features = ['GAP', 'married', 'product',
                                'channel - map', 'kvs_type',
                                'Age', 'discount', 'sex',
                                'price_gr', 'OSAGO_clnt']
    # Тергет
    target = ['target']
    # Список численных признаков
    num_features = ['KBM', 'price_prc']

    # Список признаков для обучения моделей
    filtered_features = [i for i in df.columns
                         if (i not in target and
                             i not in multicollinear_features_to_drop and
                             i not in not_correlation_features)]
    # Категориальные признаки
    cat_features = [i for i in filtered_features if i not in num_features]

    model_cbc = joblib.load(modelpath + "model_catboost.pkl")

    y_pred = model_cbc.predict(X_test)
    display(f'Accuracy: {accuracy_score(y_test, y_pred)}')


if __name__ == "__main__":
    predict()